import express, { request, response } from "express";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
import * as jwtoken from "jsonwebtoken";
import * as verifyJwt from "./verifyJwt.js";
import db from "./db";

const router = express.Router();

dotenv.config();

router.post("/auth", async (request, response, next) => {
  let username = request.body.username;
  let password = request.body.password;
  //check if the username or password is empty
  if (username === null || password === null) {
    return response.send(401).send({ message: "Missing Credentials" });
  }
  try {
    //search database for user account
    let result = await db.userLogin(username);
    if (result.length > 0) {
      //compare the hashed password with password entered
      let passwordValid = await bcrypt.compare(password, result[0].password);
      if (passwordValid) {
        const token = jwtoken.sign({ username }, process.env.JWT_SECRET, {
          expiresIn: "10m",
        });

        return response.status(200).send({ token });
      }
    }
    return response
      .status(401)
      .send({ message: "incorrect credentials provided" });
  } catch (err) {
    console.error(err);
    next(err);
  }
});
//to create new users
router.post("/users", async (request, response, next) => {
  let reqBody = request.body;
  let password = reqBody.password;
  //parse from string to int
  let saltRounds = parseInt(process.env.SALT_ROUNDS);
  try {
    //hash the password and add to the database
    let hash = await bcrypt.hash(password, saltRounds);
    if (hash) {
      reqBody.password = hash;
      await db.addUser(reqBody);
      return response
        .status(201)
        .send({ message: `${reqBody.username} created successfully` });
    }
  } catch (err) {
    console.error(err);
    next(err);
  }
});

// to add workers that were added to the users table
router.post("/worker", async (request, response, next) => {
  let reqBody = request.body;
  try {
    await db.addWorker(reqBody);
    return response
      .status(201)
      .send({ message: `${reqBody.username} added successfully` });
  } catch (err) {
    console.error(err);
    next(err);
  }
});

// to add specialist that were added to the users table
router.post("/specialist", async (request, response, next) => {
  let reqBody = request.body;
  try {
    await db.addSpecialist(reqBody);
    return response
      .status(201)
      .send({ message: `${reqBody.username} added successfully` });
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.put("/worker", async (request, response, next) => {
  let reqBody = request.body;
  try{
    await db.editWorker(reqBody);
    return response.statusCode(201).send({ message: `${reqBody.username} edited successfully` });
  }
  catch(err) {
    console.error(err);
    next(err);
  }
});

router.put("/specialist", async (request, response, next) => {
  let reqBody = request.body;
  try{
    await db.editSpecialist(reqBody);
    return response.statusCode(201).send({ message: `${reqBody.username} edited successfully` });
  }
  catch(err) {
    console.error(err);
    next(err);
  }
});

//get the list of users
router.get("/users", async (request, response, next) => {
  try {
    let result = await db.getUsers();
    return response.status(200).send(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

//get the list of workers
router.get("/worker", async (request, response) => {
  try {
    let result = await db.getWorker();
    return response.status(200).send(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

//get the list of specialist
router.get("/specialist", async (request, response) => {
  try {
    let result = await db.getSpecialist();
    return response.status(200).send(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

//get the list of admin users
router.get("/admin", async (request, response) => {
  try {
    let result = await db.getAdmin();
    return response.status(200).send(result);
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get("/viewMap", (request, response) => {});


router.post("/project", async (request, response) =>{
  let reqBody = request.body;
  try {
    await db.addProject(reqBody);
    return response
      .status(201)
      .send({ message: `${reqBody.username} added successfully` });
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get("/project", async (request, response) => {
  try {
    let result = await db.getProject();
    return response.status(200).send(result);
  }
  catch(err){
    console.error(err);
    next(err);
  }
});

router.post("/location", async (request, response) =>{
  let reqBody = request.body;
  try {
    await db.addLocation(reqBody);
    return response
      .status(201)
      .send({ message: `${reqBody.username} added successfully` });
  } catch (err) {
    console.error(err);
    next(err);
  }
});

router.get("/location", async (request, response) =>{
  try{
    let result = await db.getLocation();
    return response.status(200).send(result);
  }
  catch(err){
    console.error(err);
    next(err);
  }
});

export default router;
