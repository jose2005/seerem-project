import mysql from "mysql";
import dotenv from "dotenv";
import util from "util";

dotenv.config();

const db = mysql.createConnection({
  port: process.env.DATABASE_PORT,
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
});

const query = util.promisify(db.query).bind(db);

db.connect((error) => {
  if (error) {
    throw error;
  }
  console.log("Successfully connected to the database");
});

const userLogin = async (username) => {
  try {
    const queryResult = await query("SELECT * FROM users WHERE username = ?", [
      username,
    ]);
    return queryResult;
  } catch (err) {
    console.error(err);
  }
};

const addUser = async (reqBody) => {
  let username = reqBody.username;
  let password = reqBody.password;
  let isSpecialist = reqBody.isSpecialist;
  let isAdmin = reqBody.isAdmin;
  let employeeId = reqBody.employeeId;
  //add the user to the database
  try {
    await query("INSERT INTO users VALUES (?,?,?,?,?)", [
      employeeId,
      password,
      username,
      isSpecialist,
      isAdmin,
    ]);
  } catch (err) {
    console.error(err);
  }
};

const addWorker = async (reqBody) => {
  let username = reqBody.username;
  let site = reqBody.site;
  let supervisor = reqBody.supervisor;
  let phoneNumber = reqBody.phoneNumber;
  let workHours = reqBody.workHours;
  //add the worker to database
  try {
    await query("INSERT INTO worker VALUES (?,?,?,?,?)", [
      username,
      site,
      supervisor,
      phoneNumber,
      workHours,
    ]);
  } catch (err) {
    console.error(err);
  }
};

const editWorker = async (reqBody) => {
  let username = reqBody.username;
  let site = reqBody.site;
  let supervisor = reqBody.supervisor;
  let phoneNumber = reqBody.phoneNumber;
  let workHours = reqBody.workHours;
  //add the worker to database
  try {
    await query(
      "UPDATE worker SET site = ?, supervisor = ?, phoneNumber = ?, workHours = ? WHERE username = ?",
      [site, supervisor, phoneNumber, workHours, username]
    );
  } catch (err) {
    console.error(err);
  }
};

const addSpecialist = async (reqBody) => {
  let username = reqBody.username;
  let credential = reqBody.credential;
  let position = reqBody.position;
  let phoneNumber = reqBody.phoneNumber;
  //add the worker to database
  try {
    await query("INSERT INTO specialist VALUES (?,?,?,?)", [
      username,
      credential,
      position,
      phoneNumber,
    ]);
  } catch (err) {
    console.error(err);
  }
};

const editSpecialist = async (reqBody) => {
  let username = reqBody.username;
  let credential = reqBody.credential;
  let position = reqBody.position;
  let phoneNumber = reqBody.phoneNumber;
  //add the worker to database
  try {
    await query(
      "UPDATE specialist SET credential = ?, position = ?, phoneNumber = ? WHERE username = ?",
      [credential, position, phoneNumber, username]
    );
  } catch (err) {
    console.error(err);
  }
};

const getUsers = async () => {
  try {
    const queryResult = await query(
      "SELECT employee_id, username, isSpecialist, isAdmin FROM users"
    );
    return queryResult;
  } catch (err) {
    console.error(err);
  }
};

const getWorker = async () => {
  try {
    const queryResult = await query("SELECT * FROM worker");
    return queryResult;
  } catch (err) {
    console.error(err);
  }
};

const getSpecialist = async () => {
  try {
    const queryResult = await query("SELECT * FROM specialist");
    return queryResult;
  } catch (err) {
    console.error(err);
  }
};

const getAdmin = async () => {
  try {
    const queryResult = await query(
      "SELECT employee_id, username FROM users WHERE isAdmin = 1"
    );
    return queryResult;
  } catch (err) {
    console.error(err);
  }
};

const getProject = async () => {
  try {
    const queryResult = await query("SELECT * FROM project");
    return queryResult;
  } catch (err) {
    console.error(err);
  }
};

const addProject = async (reqBody) => {
  let projectId = reqBody.projectId;
  let location = reqBody.location;
  let description = reqBody.descriptionId;

  //add the worker to database
  try {
    await query("INSERT INTO project VALUES (?,?,?)", [
      projectId,
      location,
      description,
    ]);
  } catch (err) {
    console.error(err);
  }
};

const getLocation = async () => {
  try {
    const queryResult = await query("SELECT * FROM location");
    return queryResult;
  } catch (err) {
    console.error(err);
  }
};

const addLocation = async (reqBody) => {
  //let log_time = reqBody.log_time;
  let username = reqBody.username;
  let latitude = reqBody.latitude;
  let longitude = reqBody.longitude;

  //add the worker to database
  try {
    await query(
      "INSERT INTO location (log_time,username,latitude,longitude) VALUES (NOW(),?,?,?)",
      [username, latitude, longitude]
    );
  } catch (err) {
    console.error(err);
  }
};

export default {
  userLogin,
  addUser,
  addWorker,
  addSpecialist,
  addLocation,
  addProject,
  getProject,
  getUsers,
  getWorker,
  getSpecialist,
  getAdmin,
  getProject,
  getLocation,
  editWorker,
  editSpecialist,
};
