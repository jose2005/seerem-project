# SeeRem

Clone the Repository

Run `npm install` to install all the dependencies

Create a .env file and add the following:
```
DATABASE_HOST=*add host name*
DATABASE_USER=*username*
DATABASE_PASSWORD=*password*
DATABASE_NAME=*database name*
SALT_ROUNDS=*minimum 10 rounds*
JWT_SECRET=*add secret key*
```

To start the project run `npm start` or for developement mode run `npm run dev`

To start the project using Docker run `docker-compose up --build` 

## SQL Script

```
CREATE DATABASE seerem
USE seerem;

CREATE TABLE IF NOT EXISTS users(
	employee_id varchar(10) NOT NULL,
	password varchar(255) NOT NULL,
	username varchar(25) NOT NULL,
	isSpecialist bool NOT NULL,
	isAdmin bool NOT NULL,
	PRIMARY KEY (employee_id),
	UNIQUE(username)
);

CREATE TABLE IF NOT EXISTS worker (
	username varchar(25) NOT NULL,
	site varchar(50) NOT NULL,
	superviser varchar(255) NOT NULL,
	phone_number int(10) NOT NULL,
	work_hours float,
	PRIMARY KEY (username),
	FOREIGN KEY (username) REFERENCES users(username)
);

CREATE TABLE IF NOT EXISTS specialist (
	username varchar(255) NOT NULL,
	credential varchar(50) NOT NULL,
	position varchar(255) NOT NULL,
	phone_number int(10) NOT NULL,
	PRIMARY KEY (username),
	FOREIGN KEY (username) REFERENCES users(username)
);

CREATE TABLE IF NOT EXISTS project(
	project_id varchar(10) NOT NULL,
	location varchar(50) NOT NULL,
	Description_id varchar(255) NOT NULL,
	PRIMARY KEY (project_id)
);

CREATE TABLE IF NOT EXISTS location(
	log_id int(10) NOT NULL AUTO_INCREMENT,
	log_time datetime NOT NULL,
	username varchar(255)  NOT NULL,
	latitude FLOAT( 10, 6 ) NOT NULL ,
 	longitude FLOAT( 10, 6 ) NOT NULL,
	PRIMARY KEY (log_id),
	FOREIGN KEY (username) REFERENCES users(username)
);
```
