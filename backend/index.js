import express, { request, response } from "express";
import routes from "./src/routes";
import socket from "socket.io";
import cors from "cors";

const app = express();
const port = process.env.PORT || 3000;

//parse json
app.use(express.json());
app.use(cors());

const server = app.listen(port, () => {
  console.log(`API server ready on http://localhost:${port}`);
});

const io = socket(server);

io.on("connection", (socket) => {
  console.log(socket.id);

  socket.on("join_room", (data) => {
    socket.join(data);
    console.log("User Joined Room: " + data);
  });

  socket.on("send_message", (data) => {
    console.log(data);
    socket.to(data.room).emit("receive_message", data.content);
  });

  socket.on("disconnect", () => {
    console.log("USER DISCONNECTED");
  });
});

//add routes
app.use("/", routes);
//global error handler
app.use((err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }
  console.error(err.stack);
  return res.status(404).send({ message: "not found" });
});

export default server;
